package com.template.webserver

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import com.template.flows.LoanRequestFlow
import com.template.states.LoanRequestState
import net.corda.client.jackson.JacksonSupport
import net.corda.core.identity.CordaX500Name
import net.corda.core.messaging.startFlow
import org.springframework.web.bind.annotation.*

/**
 * Define your API endpoints here.
 */
@RestController
@RequestMapping("/") // The paths for HTTP requests are relative to this base path.
class Controller(rpc: NodeRPCConnection) {

    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }

    private val proxy = rpc.proxy

    @GetMapping(value = ["/templateendpoint"], produces = ["text/plain"])
    private fun templateendpoint(): String {
        return "Define an endpoint here."
    }

    @GetMapping(value = ["/getPeers"], produces = arrayOf("application/json"))
    private fun getPeers(): String {
        val objectMapper = JacksonSupport.createDefaultMapper(proxy)
        val nodeInfo = proxy.nodeInfo()

        val bankName = proxy.networkMapSnapshot()
                .map { it.legalIdentities.first() }
                .filter { it.name.toString().contains("Bank")}
        return objectMapper.writeValueAsString(bankName)
    }

    @PostMapping(value = ["/createRequest"], headers = arrayOf("Content-Type=application/x-www-form-urlencoded"),produces = arrayOf("application/json"))
    private fun createRequest(@RequestParam("value") requestValue: Int, @RequestParam("partyName") bank: String): String {
        val objectMapper = JacksonSupport.createDefaultMapper(proxy)
        val x500Name = CordaX500Name.parse(bank)
        val otherParty =  proxy.wellKnownPartyFromX500Name(x500Name)!!
        val flowHandle = proxy.startFlow(::LoanRequestFlow, requestValue, otherParty)
        val transactionID = flowHandle.returnValue.toCompletableFuture().get()
        return objectMapper.writeValueAsString("Success" + transactionID)
    }

    @GetMapping(value = "/myRequest", produces = arrayOf("application/json"))
    private fun myRequest(): String {
        val objectMapper = JacksonSupport.createDefaultMapper(proxy)
        val loanRequests = proxy.vaultQuery(LoanRequestState::class.java)
                .states.map { it.state.data }
        return objectMapper.writeValueAsString(loanRequests)
    }
}