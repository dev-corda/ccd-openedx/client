package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import com.template.contracts.LoanRequestContract
import com.template.states.LoanRequestState
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder


// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
class LoanRequestFlow(val requestValue: Int, val bank: Party) : FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call() : SignedTransaction  {
        // We retrieve the notary identity from the network map.
        val notary = serviceHub.networkMapCache.notaryIdentities[0]

        // We create the transaction components.
        val outputState = LoanRequestState(requestValue, ourIdentity, bank)
        val command = Command(LoanRequestContract.Commands.Request(), listOf(ourIdentity.owningKey, bank.owningKey))

        // We create a transaction builder and add the components.
        val txBuilder = TransactionBuilder(notary = notary)
                .addOutputState(outputState, LoanRequestContract.ID)
                .addCommand(command)

        // Verifying the transaction.
        txBuilder.verify(serviceHub)

        // Signing the transaction.
        val signedTx = serviceHub.signInitialTransaction(txBuilder)

        // Creating a session with the other party.
        val otherPartySession = initiateFlow(bank)

        // Obtaining the counterparty's signature.
        val fullySignedTx = subFlow(CollectSignaturesFlow(signedTx, listOf(otherPartySession),
                CollectSignaturesFlow.tracker()))

        // Finalising the transaction.
        return subFlow(FinalityFlow(fullySignedTx, otherPartySession))
    }

}

@InitiatedBy(LoanRequestFlow::class)
class LoanRequestFlowResponder(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        val signTransactionFlow = object : SignTransactionFlow(counterpartySession) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                "No inputs should be consumed when requesting loan." using (stx.tx.inputs.isEmpty())
                val output = stx.tx.outputs.single().data
                "This must be a loan request transaction." using (output is LoanRequestState)
                val loanRequest = output as LoanRequestState
                "The Loan request status should be not approved" using (loanRequest.loanStatus == 0)
            }
        }
        val expectedTxId = subFlow(signTransactionFlow).id
        subFlow(ReceiveFinalityFlow(counterpartySession, expectedTxId))
    }
}