package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import com.template.contracts.TemplateContract
import com.template.states.LoanRequestState
import net.corda.core.contracts.Command
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.TransactionBuilder


// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
class LoanRequestFlow(val requestValue: Int, val bank: Party) : FlowLogic<Unit>() {

    companion object {
        object BUILDING : ProgressTracker.Step("Building Loan Request Transaction")
        object SIGNING : ProgressTracker.Step("Signing Loan Request Transaction")
        object RECORDING : ProgressTracker.Step("Notarizing and Storing Loan Request Transaction")
    }

    // custom progress tracker
    override val progressTracker = ProgressTracker(BUILDING, SIGNING, RECORDING)

    @Suspendable
    override fun call() {
        // We retrieve the notary identity from the network map.
        val notary = serviceHub.networkMapCache.notaryIdentities[0]

        // We create the transaction components.
        val outputState = LoanRequestState(requestValue, ourIdentity, bank)
        val command = Command(TemplateContract.Commands.Action(), ourIdentity.owningKey)

        // We create a transaction builder and add the components.
		progressTracker.currentStep = BUILDING;
        val txBuilder = TransactionBuilder(notary = notary)
                .addOutputState(outputState, TemplateContract.ID)
                .addCommand(command)

        // Verifying the transaction.
        txBuilder.verify(serviceHub)

        // Signing the transaction.
		progressTracker.currentStep = SIGNING;
        val signedTx = serviceHub.signInitialTransaction(txBuilder)

        // Creating a session with the other party.
        val otherPartySession = initiateFlow(bank)

        // Finalising the transaction.
		progressTracker.currentStep = RECORDING;
        subFlow(FinalityFlow(signedTx, otherPartySession))
    }
}

@InitiatedBy(LoanRequestFlow::class)
class LoanRequestFlowResponder(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        subFlow(ReceiveFinalityFlow(counterpartySession))
    }
}