# Cordapp Client

To run the Cordapp follow the below steps:

## 1. Build the project

Windows: `gradlew deployNodes`  

Ubuntu: `./gradlew deployNodes`  

## 2. Run the network

Windows: `build\nodes\runnodes.bat`  

Ubuntu: `build/nodes/runnodes`  

## 3. Run the server for each node

`gradlew runKBAServer`

`gradlew runBankServer`
